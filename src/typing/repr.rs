/*!
`rain` representations
*/

use super::*;

/// The trait implemented by all `rain` representations
pub trait Representation: Value {
    /// Convert this value into a `ReprId`
    #[inline]
    fn into_repr(self) -> ReprId {
        ReprId::from_repr(self)
    }
    /// Get the register of this interpretation
    #[inline]
    fn repr_reg(&self) -> Option<RegRef> {
        self.try_repr_reg()
            .expect("This value has been statically asserted to be a representation!")
    }
    /// Get the type this value represents
    #[inline]
    fn repr_ty(&self) -> Option<TypeRef> {
        self.try_repr_ty()
            .expect("This value has been statically asserted to be a representation!")
    }
    /// Get the universe this type is contained in
    #[inline]
    fn universe(&self) -> VarRef<Universe> {
        self.try_universe().unwrap()
    }
}

impl<T: Type> Representation for T {}