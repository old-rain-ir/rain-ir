/*!
Typing universes
*/
use super::*;
use once_cell::sync::OnceCell;
use std::cmp::Ordering;
use std::hash::{Hash, Hasher};
use std::ops::Deref;

/// A typing universe, which is defined as being closed under pi types and sigma types.
///
/// This universe system is stratified on two levels by being parametrized with two natural numbers (here `u64`s,
/// though theoretically they should be infinite). The idea is that the `n`th universe is contained in the `n + 1`th
/// universe: to refer to all these universes, we introduce a universe `omega`. Then `omega` is contained in
/// `omega + 1`, which is contained in `omega + 2`, and all of these are contained in `omega + omega = 2omega`.
/// Hence, in general, we can represent this hierarchy as `Type(n + k * omega)`.
///
/// Universe (0, 0) is special, namely being the universe of "structural propositions." These are a class of mere
/// proposition which are treated specially by the reprretation system, namely allowing exiting the null reprretation
/// in terms of non-null reprretation.
#[derive(Debug, Clone, Eq)]
pub struct Universe {
    /// The level of this typing universe, with the parity bit denoting whether this universe admits general reprretations
    level_repr: u64,
    /// The meta-level of this typing universe
    omega: u64,
    /// The universe directly succeeding this one
    succ: OnceCell<VarId<Universe>>,
}

impl PartialEq for Universe {
    fn eq(&self, other: &Universe) -> bool {
        self.level_repr == other.level_repr && self.omega == other.omega
    }
}

impl PartialOrd for Universe {
    fn partial_cmp(&self, other: &Universe) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Universe {
    fn cmp(&self, other: &Universe) -> Ordering {
        self.omega
            .cmp(&other.omega)
            .then(self.level_repr.cmp(&other.level_repr))
    }
}

impl Hash for Universe {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.omega.hash(hasher);
        self.level_repr.hash(hasher);
    }
}

impl Universe {
    /// Get the level of this universe
    pub fn level(&self) -> u64 {
        self.level_repr.wrapping_shr(1)
    }
    /// Get the omega-level of this universe
    pub fn omega(&self) -> u64 {
        self.omega
    }
    /// Get whether this universe is pure, i.e. contains only types
    pub fn is_pure(&self) -> bool {
        self.level_repr % 2 == 0
    }
    /// Construct a new instance of the universe directly succeeding this one
    fn construct_new_succ(&self) -> Universe {
        Universe {
            level_repr: self.level_repr + 2,
            omega: self.omega,
            succ: OnceCell::new(),
        }
    }
    /// Get the universe directly succeeding this one
    fn succ(&self) -> &VarId<Universe> {
        self.succ
            .get_or_init(|| self.construct_new_succ().into_var())
    }
    /// Get the universe containing this universe and all reprretations of the types in it
    #[inline]
    pub fn repr_closure(&self) -> &Universe {
        let impure = if self.is_pure() { self.succ() } else { self };
        debug_assert!(
            !impure.is_pure(),
            "Successor {:?} to universe {:?} should not be pure!",
            impure,
            self
        );
        impure
    }
    /// Get the universe directly containing this one
    #[inline]
    pub fn containing(&self) -> &VarId<Universe> {
        self.succ().succ()
    }
}

impl Deref for VarId<Universe> {
    type Target = Universe;
    fn deref(&self) -> &Universe {
        if let ValueEnum::Universe(this) = self.as_enum() {
            this
        } else {
            unreachable!()
        }
    }
}

impl VarId<Universe> {
    /// Get the universe containing this universe and all reprretations of the types in it
    #[inline]
    pub fn repr_closure_var(&self) -> &VarId<Universe> {
        let impure = if self.is_pure() { self.succ() } else { self };
        debug_assert!(
            !impure.is_pure(),
            "Successor {:?} to universe {:?} should not be pure!",
            impure,
            self
        );
        impure
    }
}

impl<'a> VarRef<'a, Universe> {
    /// Get the universe being referenced
    #[inline]
    pub fn as_universe(&self) -> &'a Universe {
        if let ValueEnum::Universe(this) = self.as_enum() {
            this
        } else {
            unreachable!()
        }
    }
    /// Get the universe containing this universe and all reprretations of the types in it
    #[inline]
    pub fn repr_closure_ref(self) -> VarRef<'a, Universe> {
        let this = self.as_universe();
        let impure = if this.is_pure() {
            this.succ().borrow_arc()
        } else {
            self
        };
        debug_assert!(
            !impure.is_pure(),
            "Successor {:?} to universe {:?} should not be pure!",
            impure,
            self
        );
        impure
    }
    /// Get the join of this universe and another
    #[inline]
    pub fn join_ref(self, other: VarRef<'a, Universe>) -> VarRef<'a, Universe> {
        let self_u = self.as_universe();
        let other_u = other.as_universe();
        let self_bigger = self_u > other_u;
        let naive_max_pure = if self_bigger { self_u } else { other_u }.is_pure();
        let max_pure = self_u.is_pure() && other_u.is_pure();
        let needs_repr = !max_pure && naive_max_pure;
        match (self_bigger, needs_repr) {
            (false, false) => other,
            (false, true) => other_u.succ().borrow_arc(),
            (true, false) => self,
            (true, true) => self_u.succ().borrow_arc(),
        }
    }
}

impl Type for Universe {}

impl Value for Universe {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Universe(self)
    }
    #[inline(always)]
    fn into_valid(self) -> ValId {
        self.into_var().into_valid()
    }
    #[inline(always)]
    fn into_var(self) -> VarId<Universe> {
        match (self.omega, self.level_repr) {
            (0, 0) => PROP.clone(),
            (0, 1) => PROP_REPR.clone(),
            (0, 2) => TYPE.clone(),
            (0, 3) => TYPE_REPR.clone(),
            (1, 0) => OMEGA.clone(),
            _ => VarId::from_var_direct(self),
        }
    }
    #[inline(always)]
    fn repr(&self) -> ReprRef {
        self.ty().as_repr()
    }
    #[inline(always)]
    fn ty(&self) -> TypeRef {
        self.containing().borrow_as_ty()
    }
    #[inline(always)]
    fn reg(&self) -> Option<RegRef> {
        None
    }
    #[inline(always)]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline(always)]
    fn try_universe(&self) -> Result<VarRef<Universe>, Error> {
        Ok(self.containing().borrow_arc())
    }
    #[inline(always)]
    fn try_repr_ty(&self) -> Result<Option<TypeRef>, Error> {
        Ok(None)
    }
    #[inline(always)]
    fn try_repr_reg(&self) -> Result<Option<RegRef>, Error> {
        Ok(None)
    }
}

lazy_static! {
    /// The universe of structural propositions
    pub static ref PROP: VarId<Universe> = VarId::from_var_direct(Universe {
        level_repr: 0,
        omega: 0,
        succ: PROP_REPR.clone().into(),
    });
    /// The universe of structural proposition reprretations
    pub static ref PROP_REPR: VarId<Universe> = VarId::from_var_direct(Universe {
        level_repr: 1,
        omega: 0,
        succ: TYPE.clone().into()
    });
    /// The basic universe of types
    pub static ref TYPE: VarId<Universe> = VarId::from_var_direct(Universe {
        level_repr: 2,
        omega: 0,
        succ: TYPE_REPR.clone().into(),
    });
    /// The universe of types and reprretations
    pub static ref TYPE_REPR: VarId<Universe> = VarId::from_var_direct(Universe {
        level_repr: 3,
        omega: 0,
        succ: OnceCell::new()
    });
    /// The universe omega
    pub static ref OMEGA: VarId<Universe> = VarId::from_var_direct(Universe {
        level_repr: 0,
        omega: 1,
        succ: OnceCell::new()
    });
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn universe_sanity_check() {
        assert_eq!(PROP.repr(), *TYPE);
        assert_eq!(PROP.ty(), *TYPE);
        assert_eq!(PROP_REPR.repr(), *TYPE_REPR);
        assert_eq!(PROP_REPR.ty(), *TYPE_REPR);
        assert_eq!(PROP.reg(), None);
        assert_eq!(TYPE.reg(), None);
        assert_eq!(PROP_REPR.reg(), None);
        assert_eq!(TYPE_REPR.reg(), None);
        assert_eq!(PROP.level(), 0);
        assert_eq!(PROP_REPR.level(), 0);
        assert_eq!(TYPE.level(), 1);
        assert_eq!(TYPE_REPR.level(), 1);
    }
}
