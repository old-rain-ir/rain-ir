/*!
`rain` regs
*/
use super::*;

/// The trait implemented by all `rain` regs
pub trait Register: Value {
    /// Convert this value into a `RegisterId`
    #[inline]
    fn into_reg(self) -> RegId
    where
        Self: Register,
    {
        RegId::from_reg(self)
    }
}

/// The type of all register kinds.
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct RegisterKind;

impl Type for RegisterKind {}

impl Value for RegisterKind {
    /// Convert this value into a `ValueEnum`
    fn into_enum(self) -> ValueEnum {
        ValueEnum::RegisterKind(self)
    }
    /// Convert this value into a `ValId`
    fn into_valid(self) -> ValId {
        REGISTER_KIND.as_valid().clone()
    }
    /// Convert this value into a `VarId<Self>`
    fn into_var(self) -> VarId<Self> {
        REGISTER_KIND.clone()
    }
    #[inline(always)]
    fn repr(&self) -> ReprRef {
        TYPE.borrow_as_repr()
    }
    #[inline(always)]
    fn ty(&self) -> TypeRef {
        TYPE.borrow_as_ty()
    }
    #[inline(always)]
    fn reg(&self) -> Option<RegRef> {
        None
    }
}

lazy_static! {
    /// The type of all regs
    pub static ref REGISTER_KIND: VarId<RegisterKind> = VarId::from_var_direct(RegisterKind);
}
