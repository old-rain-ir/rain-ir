/*!
The `rain` type system
*/
use crate::error::*;
use crate::value::*;
use lazy_static::lazy_static;

mod register;
mod repr;
mod universe;

pub use register::*;
pub use repr::*;
pub use universe::*;

/// The trait implemented by all pure `rain` types
pub trait Type: Value {
    /// Convert this value into a `TypeId`
    #[inline]
    fn into_ty(self) -> TypeId {
        TypeId::from_ty(self)
    }
}
