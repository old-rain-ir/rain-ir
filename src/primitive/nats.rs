/*!
Natural numbers
*/
use crate::error::*;
use crate::typing::*;
use crate::value::*;
use lazy_static::lazy_static;
use num::BigUint;

/// The natural numbers type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Nats;

impl Value for Nats {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Nats(self)
    }
    #[inline(always)]
    fn into_valid(self) -> ValId {
        NATS.clone_as_valid()
    }
    #[inline(always)]
    fn into_var(self) -> VarId<Nats> {
        NATS.clone()
    }
    #[inline(always)]
    fn repr(&self) -> ReprRef {
        TYPE.borrow_as_repr()
    }
    #[inline(always)]
    fn ty(&self) -> TypeRef {
        TYPE.borrow_as_ty()
    }
    #[inline(always)]
    fn reg(&self) -> Option<RegRef> {
        None
    }
    fn is_ty(&self) -> bool {
        true
    }
    fn try_universe(&self) -> Result<VarRef<Universe>, Error> {
        Ok(TYPE.borrow_arc())
    }
}

impl Type for Nats {}

/// A constant collection of bits associated with a given type.
/// This can represent either a bitvector or a natural number
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Bits {
    /// The bits representing this constant
    data: BigUint,
    /// The representation of these constant bits
    repr: ReprId,
}

impl Value for Bits {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Bits(self)
    }
    #[inline(always)]
    fn repr(&self) -> ReprRef {
        self.repr.borrow_arc()
    }
}

lazy_static! {
    /// The type of natural numbers
    pub static ref NATS: VarId<Nats> = VarId::from_var_direct(Nats);
}
