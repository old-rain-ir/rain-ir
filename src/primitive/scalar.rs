/*!
Constants of scalar type
*/
use crate::error::*;
use crate::typing::*;
use crate::value::*;
use lazy_static::lazy_static;

/// A constant composed of an reprreted machine scalar
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Scalar {
    /// The underlying machine scalar
    data: ScalarData,
    /// The representation of this scalar
    repr: ReprId,
}

impl Scalar {
    /// Create a new scalar constant under the trivial representation
    #[inline]
    pub fn new_trivial(data: ScalarData) -> Scalar {
        Scalar {
            data,
            repr: ScalarRepr::from(data.kind()).into_repr(),
        }
    }
}

impl From<ScalarData> for Scalar {
    fn from(data: ScalarData) -> Scalar {
        Scalar::new_trivial(data)
    }
}

impl From<bool> for Scalar {
    #[inline]
    fn from(b: bool) -> Scalar {
        Scalar::new_trivial(b.into())
    }
}

impl From<u8> for Scalar {
    #[inline]
    fn from(u: u8) -> Scalar {
        Scalar::new_trivial(u.into())
    }
}

impl From<u16> for Scalar {
    #[inline]
    fn from(u: u16) -> Scalar {
        Scalar::new_trivial(u.into())
    }
}

impl From<u32> for Scalar {
    #[inline]
    fn from(u: u32) -> Scalar {
        Scalar::new_trivial(u.into())
    }
}

impl From<u64> for Scalar {
    #[inline]
    fn from(u: u64) -> Scalar {
        Scalar::new_trivial(u.into())
    }
}

impl Value for Scalar {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Scalar(self)
    }
    #[inline(always)]
    fn into_valid(self) -> ValId {
        ValId::new_direct(ValueEnum::Scalar(self))
    }
    #[inline(always)]
    fn into_var(self) -> VarId<Scalar> {
        VarId::from_var_direct(self)
    }
    #[inline(always)]
    fn repr(&self) -> ReprRef {
        self.repr.borrow_arc()
    }
}

/// A finite machine scalar
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Finite {
    /// The maximum value of this scalar's type
    max: u64,
    /// The value of this particular scalar, which must be <= the maximum value
    value: u64,
}

impl Finite {
    /// The true boolean value in the default representation
    pub const TRUE: Finite = Finite { max: 1, value: 1 };
    /// The false boolean value in the default representation
    pub const FALSE: Finite = Finite { max: 1, value: 0 };
    /// Construct a new finite machine scalar, returning an error if `value > max`
    #[inline]
    pub fn new(value: u64, max: u64) -> Result<Finite, Error> {
        if value > max {
            return Err(Error::FiniteOutOfBounds("Finite::new"));
        }
        Ok(Finite { max, value })
    }
}

/// A machine scalar
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum ScalarData {
    /// A finite-valued scalar
    Finite(Finite),
    /// A boolean value
    Bool(bool),
    /// A byte, or 8-bit machine scalar
    U8(u8),
    /// A 16-bit machine scalar (short)
    U16(u16),
    /// A 32-bit machine scalar (int)
    U32(u32),
    /// A 64-bit machine scalar (long)
    U64(u64),
}

impl ScalarData {
    /// The true boolean value in the default representation
    pub const TRUE: ScalarData = ScalarData::Finite(Finite::TRUE);
    /// The false boolean value in the default representation
    pub const FALSE: ScalarData = ScalarData::Finite(Finite::FALSE);
    /// Get the kind of this scalar data
    pub fn kind(self) -> ScalarKind {
        match self {
            ScalarData::Finite(f) => ScalarKind::Finite(f.max),
            ScalarData::Bool(_) => ScalarKind::Bool,
            ScalarData::U8(_) => ScalarKind::U8,
            ScalarData::U16(_) => ScalarKind::U16,
            ScalarData::U32(_) => ScalarKind::U32,
            ScalarData::U64(_) => ScalarKind::U64,
        }
    }
}

impl From<bool> for ScalarData {
    #[inline]
    fn from(b: bool) -> ScalarData {
        ScalarData::Bool(b)
    }
}

impl From<u8> for ScalarData {
    #[inline]
    fn from(u: u8) -> ScalarData {
        ScalarData::U8(u)
    }
}

impl From<u16> for ScalarData {
    #[inline]
    fn from(u: u16) -> ScalarData {
        ScalarData::U16(u)
    }
}

impl From<u32> for ScalarData {
    #[inline]
    fn from(u: u32) -> ScalarData {
        ScalarData::U32(u)
    }
}

impl From<u64> for ScalarData {
    #[inline]
    fn from(u: u64) -> ScalarData {
        ScalarData::U64(u)
    }
}

/// Machine scalar kinds
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum ScalarKind {
    /// A finite-valued scalar kind
    Finite(u64),
    /// A boolean value
    Bool,
    /// A byte, or 8-bit machine scalar
    U8,
    /// A 16-bit machine scalar (short)
    U16,
    /// A 32-bit machine scalar (int)
    U32,
    /// A 64-bit machine scalar (long)
    U64,
}

/// Machine scalar types
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct ScalarTy(pub ScalarKind);

impl Value for ScalarTy {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::ScalarTy(self)
    }
    #[inline(always)]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline(always)]
    fn is_repr(&self) -> bool {
        true
    }
    #[inline(always)]
    fn repr(&self) -> ReprRef {
        TYPE.borrow_as_repr()
    }
    #[inline]
    fn try_universe(&self) -> Result<VarRef<Universe>, Error> {
        Ok(TYPE.borrow_arc())
    }
    #[inline]
    fn try_repr_ty(&self) -> Result<Option<TypeRef>, Error> {
        Ok(None)
    }
    #[inline]
    fn try_repr_reg(&self) -> Result<Option<RegRef>, Error> {
        Ok(None)
    }
}

impl Type for ScalarTy {}

/// Machine scalar registers
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct ScalarReg(pub ScalarKind);

impl Value for ScalarReg {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::ScalarReg(self)
    }
    #[inline(always)]
    fn repr(&self) -> ReprRef {
        REGISTER_KIND.borrow_as_repr()
    }
    #[inline(always)]
    fn is_reg(&self) -> bool {
        true
    }
}

impl Register for ScalarReg {}

/// Direct machine scalar reprretations over a given scalar type
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct ScalarRepr {
    ty: VarId<ScalarTy>,
    reg: VarId<ScalarReg>,
}

impl From<ScalarKind> for ScalarRepr {
    fn from(kind: ScalarKind) -> ScalarRepr {
        ScalarRepr {
            ty: ScalarTy(kind).into_var(),
            reg: ScalarReg(kind).into_var(),
        }
    }
}

impl Value for ScalarRepr {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::ScalarRepr(self)
    }
    #[inline(always)]
    fn is_repr(&self) -> bool {
        true
    }
    #[inline(always)]
    fn repr(&self) -> ReprRef {
        TYPE_REPR.borrow_as_repr()
    }
    #[inline]
    fn try_universe(&self) -> Result<VarRef<Universe>, Error> {
        Ok(TYPE_REPR.borrow_arc())
    }
    #[inline]
    fn try_repr_ty(&self) -> Result<Option<TypeRef>, Error> {
        Ok(Some(self.ty.borrow_as_ty()))
    }
    #[inline]
    fn try_repr_reg(&self) -> Result<Option<RegRef>, Error> {
        Ok(Some(self.reg.borrow_as_reg()))
    }
}

impl Representation for ScalarRepr {}
impl Representation for VarId<ScalarRepr> {}
impl Representation for VarRef<'_, ScalarRepr> {}

lazy_static! {
    /// The type of booleans
    pub static ref BOOL_TY: VarId<ScalarTy> = VarId::from_var_direct(ScalarTy(ScalarKind::Bool));
    /// The boolean register kind
    pub static ref BOOL_REG: VarId<ScalarReg> = VarId::from_var_direct(ScalarReg(ScalarKind::Bool));
    /// The boolean representation
    pub static ref BOOL: VarId<ScalarRepr> = VarId::from_var_direct(ScalarRepr::from(ScalarKind::Bool));
    /// The boolean constant true
    pub static ref TRUE: VarId<Scalar> = VarId::from_var_direct(Scalar::from(true));
    /// The boolean constant false
    pub static ref FALSE: VarId<Scalar> = VarId::from_var_direct(Scalar::from(false));
    /// The type of bytes
    pub static ref U8_TY: VarId<ScalarTy> = VarId::from_var_direct(ScalarTy(ScalarKind::U8));
    /// The byte register kind
    pub static ref U8_REG: VarId<ScalarReg> = VarId::from_var_direct(ScalarReg(ScalarKind::U8));
    /// The byte reprESENTATION
    pub static ref U8: VarId<ScalarRepr> = VarId::from_var_direct(ScalarRepr::from(ScalarKind::U8));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn scalar_sanity_check() {
        assert_eq!(BOOL.repr_ty().unwrap(), *BOOL_TY);
        assert_eq!(U8.repr_ty().unwrap(), *U8_TY);
        assert_eq!(BOOL_TY.repr_ty(), None);
        assert_eq!(U8_TY.repr_ty(), None);
        assert_eq!(TRUE.repr(), *BOOL);
        assert_eq!(FALSE.repr(), *BOOL);
        assert_eq!(TRUE.ty(), *BOOL_TY);
        assert_eq!(FALSE.ty(), *BOOL_TY);
        assert_eq!(TRUE.reg().unwrap(), *BOOL_REG);
        assert_eq!(FALSE.reg().unwrap(), *BOOL_REG);
        assert!(BOOL_TY.is_ty());
        assert!(BOOL_TY.is_repr());
        assert!(!BOOL_TY.is_reg());
        assert!(!BOOL_REG.is_ty());
        assert!(!BOOL_REG.is_repr());
        assert!(BOOL_REG.is_reg());
        assert!(!BOOL.is_ty());
        assert!(BOOL.is_repr());
        assert!(!BOOL.is_reg());
    }
}
