/*!
An S-expression, representing eager function application.
*/
use super::*;

/// An S-expression, representing eager function application to one argument
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Sexpr {
    /// The function being applied
    func: ValId,
    /// The argument it is being applied to
    arg: ValId,
    /// The representation of this S-expression as a value
    repr: ReprId,
    /// The free variables present in this S-expression
    fvs: ValSet,
}

impl Value for Sexpr {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Sexpr(self)
    }
    #[inline(always)]
    fn repr(&self) -> ReprRef {
        self.repr.borrow_arc()
    }
    #[inline(always)]
    fn fv(&self) -> &ValSet {
        &self.fvs
    }
    fn is_ty(&self) -> bool {
        unimplemented!("Sexpr types")
    }
}