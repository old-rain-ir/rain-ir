/*!
Pi types, i.e. dependent function types
*/
use super::*;

/// A pi type
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Pi {
    /// The variable which the result type depends on
    var: SymbolId,
    /// The representation of the result
    repr: ReprId,
    /// The type of this pi type
    ty: VarId<Universe>,
    /// The free variables of this pi type
    fvs: ValSet,
}

impl Pi {
    /// Create a new pi type by abstracting a given representation over a given symbol
    #[inline]
    pub fn new(var: SymbolId, repr: ReprId) -> Pi {
        let var_ty = var.ty();
        let var_universe = var_ty.universe();
        let repr_universe = repr.universe();
        let ty = var_universe.join_ref(repr_universe).clone_as_arc();
        //TODO: free variables
        Pi { var, repr, ty, fvs: ValSet::EMPTY }
    }
    /// Create a pi type which matches the identity for a given symbol
    #[inline]
    pub fn id(var: SymbolId) -> Pi {
        let repr = var.clone_repr();
        Self::new(var, repr)
    }
}

impl Value for Pi {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Pi(self)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        ValId::new_direct(ValueEnum::Pi(self))
    }
    #[inline]
    fn into_var(self) -> VarId<Self> {
        VarId::from_var_direct(self)
    }
    #[inline]
    fn repr(&self) -> ReprRef {
        self.ty.borrow_as_repr()
    }
    #[inline]
    fn ty(&self) -> TypeRef {
        self.ty.borrow_as_ty()
    }
    #[inline]
    fn reg(&self) -> Option<RegRef> {
        None
    }
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn try_universe(&self) -> Result<VarRef<Universe>, Error> {
        Ok(self.ty.borrow_arc())
    }
    /// Try to get the type this value represents if it is a representation
    #[inline]
    fn try_repr_ty(&self) -> Result<Option<TypeRef>, Error> {
        Ok(None)
    }
    /// Try to get the reg of this value, if any, as a representation if it is a representaton
    #[inline]
    fn try_repr_reg(&self) -> Result<Option<RegRef>, Error> {
        Ok(None)
    }
}

impl Type for Pi {}
