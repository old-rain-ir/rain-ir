/*!
Lambda abstractions
*/
use super::*;

/// A lambda abstraction
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Lambda {
    /// The variable being abstracted over
    var: SymbolId,
    /// The result of this lambda function
    result: ValId,
    /// The representation of this lambda function
    repr: ReprId,
    /// The free variables of this lambda function
    fvs: ValSet,
}

impl Lambda {
    /// Construct a new lambda abstraction in M-mode, i.e. without a representation
    pub fn new_abstract(var: SymbolId, result: ValId) -> Lambda {
        let repr = Pi::new(var.clone(), result.clone_repr()).into_repr();
        //TODO: free variables
        Lambda { var, result, repr, fvs: ValSet::EMPTY }
    }
    /// Create the *abstract* identity function over a given symbol
    pub fn id_abstract(var: SymbolId) -> Lambda {
        let result = var.clone().into_valid();
        Self::new_abstract(var, result)
    }
}

impl Value for Lambda {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Lambda(self)
    }
    fn into_valid(self) -> ValId {
        ValId::new_direct(self.into_enum())
    }
    fn repr(&self) -> ReprRef {
        self.repr.borrow_arc()
    }
    fn fv(&self) -> &ValSet {
        &self.fvs
    }
}
