/*!
Error handling for the `rain` frontend.z
*/
use failure::Fail;

/// A `rain` error
#[derive(Debug, Copy, Clone, Fail)]
pub enum Error {
    /// Attempted to apply a value which is not a function
    #[fail(display = "Attempted to apply a value which is not a function: {}", 0)]
    NotAFunction(&'static str),
    /// Attempted to apply a function to a value of the wrong type
    #[fail(display = "Attempted to apply a function to a value of the wrong type: {}", 0)]
    TypeMismatch(&'static str),
    /// Attempted to use an invalid value as a representation
    #[fail(display = "Attempted to use an invalid value as a representation: {}", 0)]
    NotARepr(&'static str),
    /// Attempted to use an invalid value as a type
    #[fail(display = "Attempted to use an invalid value as a type: {}", 0)]
    NotAType(&'static str),
    /// Attempted to use an invalid value as a register
    #[fail(display = "Attempted to use an invalid value as a register: {}", 0)]
    NotARegister(&'static str),
    /// Attempted to construct a finite value out of bounds
    #[fail(display = "Attempted to construct an out-of-bounds finite value: {}", 0)]
    FiniteOutOfBounds(&'static str)
}
