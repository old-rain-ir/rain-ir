/*!
The main `rain` value data structure
*/
use elysees::{Arc, ArcBorrow};
use std::marker::PhantomData;

mod pred;
mod set;
mod symbol;
mod valid_impl;
pub use pred::*;
pub use set::*;
pub use symbol::*;
pub use valid_impl::*;

use crate::error::*;
use crate::function::*;
use crate::primitive::*;
use crate::typing::*;
use nats::*;
use scalar::*;

/// A `rain` value
#[derive(Clone)]
#[repr(transparent)]
pub struct ValId<P = ()> {
    /// The data backing this value
    ptr: Arc<ValueEnum>,
    /// The predicate the data is asserted to satisfy
    pred: PhantomData<P>,
}

/// A borrowed `rain` value
#[repr(transparent)]
pub struct ValRef<'a, P = ()> {
    /// The data backing this value
    ptr: ArcBorrow<'a, ValueEnum>,
    /// The predicate the data is asserted to satisfy
    pred: PhantomData<P>,
}

/// A `rain` type
pub type TypeId = ValId<IsType>;

/// A `rain` reg
pub type RegId = ValId<IsReg>;

/// A `rain` representation
pub type ReprId = ValId<IsRepr>;

/// A `rain` value asserted to be a given variant
pub type VarId<V> = ValId<IsA<V>>;

/// A borrowed `rain` type
pub type TypeRef<'a> = ValRef<'a, IsType>;

/// A borrowed `rain` reg
pub type RegRef<'a> = ValRef<'a, IsReg>;

/// A borrowed `rain` representation
pub type ReprRef<'a> = ValRef<'a, IsRepr>;

/// A borrowed `rain` value asserted to be a given variant
pub type VarRef<'a, V> = ValRef<'a, IsA<V>>;

/// The trait implemented by all `rain` values
pub trait Value: Sized {
    /// Convert this value into a `ValueEnum`
    fn into_enum(self) -> ValueEnum;
    /// Convert this value into a `ValId`
    fn into_valid(self) -> ValId {
        ValId::new_direct(self.into_enum())
    }
    /// Convert this value into a `VarId<Self>`
    #[inline]
    fn into_var(self) -> VarId<Self> {
        ValId::from_var(self)
    }
    /// Get the set of free variables of this value
    fn fv(&self) -> &ValSet {
        &EMPTY_SET
    }
    /// Get the reprretation of this value
    fn repr(&self) -> ReprRef;
    /// Get the type of this value
    #[inline]
    fn ty(&self) -> TypeRef {
        let repr = self.repr();
        if let Some(ty) = repr
            .as_enum()
            .try_repr_ty()
            .expect("Invalid value reprretation")
        {
            ty
        } else {
            debug_assert!(repr.is_ty());
            repr.cast_ref_unchecked()
        }
    }
    /// Clone the reprretation of this value
    fn clone_repr(&self) -> ReprId {
        self.repr().clone_as_arc()
    }
    /// Clone the type of this value
    fn clone_ty(&self) -> TypeId {
        self.ty().clone_as_arc()
    }
    /// Get the reg of this value, if any
    #[inline]
    fn reg(&self) -> Option<RegRef> {
        self.repr()
            .as_enum()
            .try_repr_reg()
            .expect("Invalid value representation")
    }
    /// Get whether this value is a type
    #[inline]
    fn is_ty(&self) -> bool {
        false
    }
    /// Get whether this value is a register
    #[inline]
    fn is_reg(&self) -> bool {
        false
    }
    /// Get whether this value is a representation
    #[inline]
    fn is_repr(&self) -> bool {
        false
    }
    /// If this value is a representation, get the universe it is contained in, otherwise return an error
    #[inline]
    fn try_universe(&self) -> Result<VarRef<Universe>, Error> {
        Err(Error::NotARepr("try_universe for Value"))
    }
    /// Try to get the type this value represents if it is a representation, or `None` if it is this object
    #[inline]
    fn try_repr_ty(&self) -> Result<Option<TypeRef>, Error> {
        Err(Error::NotARepr("try_repr_ty for Value"))
    }
    /// Try to get the reg of this value, if any, as a representation if it is a representaton
    #[inline]
    fn try_repr_reg(&self) -> Result<Option<RegRef>, Error> {
        Err(Error::NotARegister("try_repr_reg for Value"))
    }
}

/// An enumeration of all possible `rain` values
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum ValueEnum {
    // === Fundamental value and type constructors ===
    /// A symbol
    Symbol(Symbol),
    /// An S-expression
    Sexpr(Sexpr),
    /// A pi type
    Pi(Pi),
    /// A lambda function
    Lambda(Lambda),

    // === Primitive types ===
    /// A constant machine scalar
    Scalar(Scalar),
    /// A machine scalar type
    ScalarTy(ScalarTy),
    /// A constant collection of bits
    Bits(Bits),
    /// The type of natural numbers
    Nats(Nats),

    // === Typing universes and identity types ===
    /// The type of types
    Universe(Universe),

    // === Interpretations and registers ===
    /// The type of all registers
    RegisterKind(RegisterKind),
    /// Scalar registers
    ScalarReg(ScalarReg),
    /// Direct scalar reprretations
    ScalarRepr(ScalarRepr),
}

/// A macro to apply the same expression to every variant of the `ValueEnum`
macro_rules! forv {
    ($v:expr ; $i:ident => $e:expr) => {
        match $v {
            ValueEnum::Symbol($i) => $e,
            ValueEnum::Sexpr($i) => $e,
            ValueEnum::Pi($i) => $e,
            ValueEnum::Lambda($i) => $e,

            ValueEnum::Scalar($i) => $e,
            ValueEnum::ScalarTy($i) => $e,
            ValueEnum::Bits($i) => $e,
            ValueEnum::Nats($i) => $e,

            ValueEnum::Universe($i) => $e,

            ValueEnum::RegisterKind($i) => $e,
            ValueEnum::ScalarReg($i) => $e,
            ValueEnum::ScalarRepr($i) => $e,
        }
    };
}

impl Value for ValueEnum {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        self
    }
    fn into_valid(self) -> ValId {
        forv!(self ; v => v.into_valid())
    }
    fn repr(&self) -> ReprRef {
        forv!(self ; v => v.repr())
    }
    fn ty(&self) -> TypeRef {
        forv!(self ; v => v.ty())
    }
    fn reg(&self) -> Option<RegRef> {
        forv!(self ; v => v.reg())
    }
    fn is_ty(&self) -> bool {
        forv!(self ; v => v.is_ty())
    }
    fn is_reg(&self) -> bool {
        forv!(self ; v => v.is_reg())
    }
    fn is_repr(&self) -> bool {
        forv!(self ; v => v.is_repr())
    }
    fn try_universe(&self) -> Result<VarRef<Universe>, Error> {
        forv!(self ; v => v.try_universe())
    }
    fn try_repr_ty(&self) -> Result<Option<TypeRef>, Error> {
        forv!(self ; v => v.try_repr_ty())
    }
    fn try_repr_reg(&self) -> Result<Option<RegRef>, Error> {
        forv!(self ; v => v.try_repr_reg())
    }
}
