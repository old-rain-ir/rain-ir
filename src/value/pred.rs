/*!
Predicates for `rain` `ValId`s.
*/

use super::*;

/// A predicate asserting a value is a valid `rain` type.
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd, Default)]
pub struct IsType;

/// A predicate asserting a value is a valid `rain` register.
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd, Default)]
pub struct IsReg;

/// A predicate asserting a value is a valid `rain` representation.
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd, Default)]
pub struct IsRepr;

/// A predicate asserting a value is of a given type
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd, Default)]
pub struct IsA<V>(pub PhantomData<V>);
