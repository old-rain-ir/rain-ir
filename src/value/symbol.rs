/*!
Abstract `rain` symbols
*/
use super::*;
use elysees::ArcBox;

/// An abstract symbol having a given representation
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Symbol {
    /// The representation of this symbol
    repr: ReprId,
    /// The address of this symbol, reflected back into this symbol as a `usize`
    /// 
    /// We store the address in the symbol to preserve the usual fact that two
    /// objects are structurally equal if and only if their `ValId`s are pointer
    /// equal.
    addr: usize,
}

impl Symbol {
    /// Create a new symbol with a given representation
    pub fn new<R: Representation>(repr: R) -> SymbolId {
        let mut arc = ArcBox::new(ValueEnum::Symbol(Symbol {
            repr: repr.into_repr(),
            addr: 0,
        }));
        let addr = &*arc as *const _ as usize;
        match &mut *arc {
            ValueEnum::Symbol(s) => s.addr = addr,
            _ => unreachable!(),
        }
        ValId::dedup(arc.shareable()).cast_valid_unchecked()
    }
}

impl Value for Symbol {
    /// Convert this value into a `ValueEnum`
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Symbol(self)
    }
    /// Get the representation of this value
    #[inline]
    fn repr(&self) -> ReprRef {
        self.repr.borrow_arc()
    }
    /// Try to get the type this value represents if it is a representation, or `None` if it is this object.
    #[inline]
    fn try_repr_ty(&self) -> Result<Option<TypeRef>, Error> {
        unimplemented!("Symbolic representation")
    }
    /// Try to get the reg of this value, if any, as a representation if it is a representaton
    #[inline]
    fn try_repr_reg(&self) -> Result<Option<RegRef>, Error> {
        unimplemented!("Symbolic reg")
    }
}

/// An ID for an abstract symbol having a given representation
pub type SymbolId = VarId<Symbol>;
