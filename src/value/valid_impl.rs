/*!
Implementation details for `ValId`
*/
use super::*;
use cons::{flurry_cons::Cache, Cons, Uncons};
use lazy_static::lazy_static;
use std::borrow::Borrow;
use std::cmp::Ordering;
use std::fmt::{self, Debug, Formatter};
use std::hash::{Hash, Hasher};
use std::ops::Deref;

impl<P> Value for ValId<P> {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        (*self.ptr).clone()
    }
    fn into_valid(self) -> ValId {
        self.cast_valid_unchecked()
    }
    fn repr(&self) -> ReprRef {
        self.ptr.repr()
    }
    fn is_ty(&self) -> bool {
        self.ptr.is_ty()
    }
    fn is_repr(&self) -> bool {
        self.ptr.is_repr()
    }
    fn is_reg(&self) -> bool {
        self.ptr.is_reg()
    }
    fn ty(&self) -> TypeRef {
        self.ptr.ty()
    }
    fn reg(&self) -> Option<RegRef> {
        self.ptr.reg()
    }
    fn try_repr_ty(&self) -> Result<Option<TypeRef>, Error> {
        self.ptr.try_repr_ty()
    }
    fn try_repr_reg(&self) -> Result<Option<RegRef>, Error> {
        self.ptr.try_repr_reg()
    }
}

impl<P> Debug for ValId<P> {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        Debug::fmt(&self.ptr, fmt)
    }
}

impl<P> Debug for ValRef<'_, P> {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        Debug::fmt(&self.ptr, fmt)
    }
}

impl<V: Type> Type for VarId<V> {}

impl Type for TypeId {}

impl<V: Register> Register for VarId<V> {}

impl Register for RegId {}

impl Representation for ReprId {}

impl<'a, P> Value for ValRef<'a, P> {
    #[inline(always)]
    fn into_enum(self) -> ValueEnum {
        (*self.ptr).clone()
    }
    fn into_valid(self) -> ValId {
        self.clone_as_valid()
    }
    fn repr(&self) -> ReprRef {
        self.ptr.repr()
    }
    fn ty(&self) -> TypeRef {
        self.ptr.ty()
    }
    fn reg(&self) -> Option<RegRef> {
        self.ptr.reg()
    }
    fn try_repr_ty(&self) -> Result<Option<TypeRef>, Error> {
        self.ptr.try_repr_ty()
    }
    fn try_repr_reg(&self) -> Result<Option<RegRef>, Error> {
        self.ptr.try_repr_reg()
    }
}

impl<V: Type> Type for VarRef<'_, V> {}

impl Type for TypeRef<'_> {}

impl<V: Register> Register for VarRef<'_, V> {}

impl Register for RegRef<'_> {}

impl Representation for ReprRef<'_> {}

lazy_static! {
    /// The cache of hash-consed `rain` values
    pub static ref VALUE_CACHE: Cache<Arc<ValueEnum>> = Cache::new();
}

impl ValId {
    /// Deduplicate an `Arc<ValueEnum>` into a `ValId`
    pub fn dedup(arc: Arc<ValueEnum>) -> ValId {
        let ptr = VALUE_CACHE.cons(arc);
        ValId {
            ptr,
            pred: PhantomData,
        }
    }
    /// Directly cons a `ValueEnum` into a `ValId`
    pub fn new_direct(value: ValueEnum) -> ValId {
        let ptr = VALUE_CACHE.cons(value);
        ValId {
            ptr,
            pred: PhantomData,
        }
    }
}

impl<P> Drop for ValId<P> {
    fn drop(&mut self) {
        VALUE_CACHE.gc(&mut self.ptr);
    }
}

impl<P> ValId<P> {
    /// Get the `ValueEnum` underlying this `ValId`
    pub fn as_enum(&self) -> &ValueEnum {
        &*self.ptr
    }
    /// Get a pointer to the `ValueEnum` underlying this `ValId`
    ///
    /// # Example
    /// ```rust
    /// use rain_ir::primitive::scalar::TRUE;
    /// assert_eq!(TRUE.as_enum() as *const _, TRUE.as_ptr());
    /// ```
    pub fn as_ptr(&self) -> *const ValueEnum {
        Arc::as_ptr(&self.ptr)
    }
    /// Get this `ValId<P>` as a `ValId`
    pub fn as_valid(&self) -> &ValId {
        ValId::cast_arc_ref(&self.ptr)
    }
    /// Get this `ValId<P>` as a `ReprId`
    pub fn as_repr(&self) -> &ReprId
    where
        Self: Representation,
    {
        ValId::cast_arc_ref(&self.ptr)
    }
    /// Get this `ValId<P>` as a `TypeId`
    pub fn as_ty(&self) -> &TypeId
    where
        Self: Type,
    {
        ValId::cast_arc_ref(&self.ptr)
    }
    /// Get this `ValId<P>` as a `RegId`
    pub fn as_reg(&self) -> &RegId
    where
        Self: Register,
    {
        ValId::cast_arc_ref(&self.ptr)
    }
    /// Borrow this `ValId<P>` as a `ValRef<P>`
    pub fn borrow_arc(&self) -> ValRef<P> {
        ValRef {
            ptr: Arc::borrow_arc(&self.ptr),
            pred: PhantomData,
        }
    }
    /// Borrow this `ValId<P>` as a `ValRef`
    pub fn borrow_as_valid(&self) -> ValRef {
        ValRef {
            ptr: Arc::borrow_arc(&self.ptr),
            pred: PhantomData,
        }
    }
    /// Borrow this `ValId<P>` as a `ReprRef`
    pub fn borrow_as_repr(&self) -> ReprRef
    where
        Self: Representation,
    {
        ValRef {
            ptr: Arc::borrow_arc(&self.ptr),
            pred: PhantomData,
        }
    }
    /// Borrow this `ValId<P>` as a `TypeRef`
    pub fn borrow_as_ty(&self) -> TypeRef
    where
        Self: Type,
    {
        ValRef {
            ptr: Arc::borrow_arc(&self.ptr),
            pred: PhantomData,
        }
    }
    /// Borrow this `ValId<P>` as a `RegRef`
    pub fn borrow_as_reg(&self) -> RegRef
    where
        Self: Register,
    {
        ValRef {
            ptr: Arc::borrow_arc(&self.ptr),
            pred: PhantomData,
        }
    }
    /// Clone this `ValRef<P>` into a `ValId<P>`
    #[inline(always)]
    pub fn clone_as_arc(&self) -> ValId<P> {
        ValId {
            ptr: self.ptr.clone(),
            pred: PhantomData,
        }
    }
    /// Clone this `ValRef<P>` as a `ValId`
    #[inline(always)]
    pub fn clone_as_valid(&self) -> ValId {
        ValId {
            ptr: self.ptr.clone(),
            pred: PhantomData,
        }
    }
    /// Clone this `ValRef<P>` as a `TypeId`
    #[inline(always)]
    pub fn clone_as_ty(&self) -> TypeId
    where
        Self: Type,
    {
        ValId {
            ptr: self.ptr.clone(),
            pred: PhantomData,
        }
    }
    /// Clone this `ValRef<P>` as a `RegId`
    #[inline(always)]
    pub fn clone_as_reg(&self) -> RegId
    where
        Self: Register,
    {
        ValId {
            ptr: self.ptr.clone(),
            pred: PhantomData,
        }
    }
    /// Clone this `ValRef<P>` as a `ReprId`
    #[inline(always)]
    pub fn clone_as_repr(&self) -> ReprId
    where
        Self: Representation,
    {
        ValId {
            ptr: self.ptr.clone(),
            pred: PhantomData,
        }
    }
    /// Get the underlying `Arc<ValueEnum>` behind this `ValId`.
    ///
    /// This method ensures there can be no leaks
    #[inline]
    pub fn into_arc(mut self) -> Arc<ValueEnum> {
        VALUE_CACHE.gc(&mut self.ptr);
        self.into_arc_direct()
    }
    /// Get the underlying `Arc<ValueEnum>` behind this `ValId`.
    ///
    /// Note that this may cause a leak in the garbage collector if this is the last reference to an `Arc<ValueEnum>`
    /// outside the global cons-table and it is not used to construct another `ValId`. To avoid this, use `into_arc`.
    #[inline(always)]
    pub fn into_arc_direct(self) -> Arc<ValueEnum> {
        // Safety: by `#[repr(transparent)]`, `Arc<ValueEnum>` and `ValId<P>` are guaranteed the same reg.
        // We can't just do `self.ptr` because of the `Drop` impl on` ValId`, which here we specifically wish to elide.
        unsafe { std::mem::transmute(self) }
    }
    /// Cast this `ValId<P>` into a `ValId<Q>` without performing any checks.
    /// May break correctness guarantees, but safe
    #[inline(always)]
    pub(super) fn cast_valid_unchecked<Q>(self) -> ValId<Q> {
        ValId {
            ptr: self.into_arc_direct(),
            pred: PhantomData,
        }
    }
    /// Cast an `&Arc<ValueEnum>` into a `&ValId<P>` without performing any checks.
    /// May break correctness guarantees, but safe.
    #[inline(always)]
    pub(super) fn cast_arc_ref(arc: &Arc<ValueEnum>) -> &ValId<P> {
        // Safety: by `#[repr(transparent)]`, `Arc<ValueEnum>` and `ValId<P>` are guaranteed the same reg.
        unsafe { &*(arc as *const _ as *const ValId<P>) }
    }
}

impl<'a, P> ValRef<'a, P> {
    /// Get the `ValueEnum` underlying this `ValRef`
    pub fn as_enum(&self) -> &'a ValueEnum {
        self.ptr.get()
    }
    /// Get this `ValRef<P>` as a `ValRef`
    pub fn as_valid(&self) -> ValRef<'a> {
        self.cast_ref_unchecked()
    }
    /// Get this `ValRef<P>` as a `ReprRef`
    pub fn as_repr(&self) -> ReprRef<'a>
    where
        Self: Representation,
    {
        self.cast_ref_unchecked()
    }
    /// Get this `ValRef<P>` as a `TypeRef`
    pub fn as_ty(&self) -> TypeRef<'a>
    where
        Self: Type,
    {
        self.cast_ref_unchecked()
    }
    /// Get this `ValRef<P>` as a `RegRef`
    pub fn as_reg(&self) -> RegRef<'a>
    where
        Self: Register,
    {
        self.cast_ref_unchecked()
    }
    /// Clone this `ValRef<P>` into a `ValId<P>`
    #[inline(always)]
    pub fn clone_as_arc(self) -> ValId<P> {
        ValId {
            ptr: self.ptr.clone_arc(),
            pred: PhantomData,
        }
    }
    /// Clone this `ValRef<P>` into a `ValId`
    #[inline(always)]
    pub fn clone_as_valid(self) -> ValId {
        ValId {
            ptr: self.ptr.clone_arc(),
            pred: PhantomData,
        }
    }
    /// Clone this `ValRef<P>` as a `TypeId`
    #[inline(always)]
    pub fn clone_as_ty(self) -> TypeId
    where
        Self: Type,
    {
        ValId {
            ptr: self.ptr.clone_arc(),
            pred: PhantomData,
        }
    }
    /// Clone this `ValRef<P>` as a `RegId`
    #[inline(always)]
    pub fn clone_as_reg(self) -> RegId
    where
        Self: Register,
    {
        ValId {
            ptr: self.ptr.clone_arc(),
            pred: PhantomData,
        }
    }
    /// Clone this `ValRef<P>` as a `ReprId`
    #[inline(always)]
    pub fn clone_as_repr(self) -> ReprId
    where
        Self: Representation,
    {
        ValId {
            ptr: self.ptr.clone_arc(),
            pred: PhantomData,
        }
    }
    /// Cast this `ValRef<P>` into a `ValRef<Q>` without performing any checks.
    /// May break correctness guarantees, but safe
    #[inline(always)]
    pub(super) fn cast_ref_unchecked<Q>(self) -> ValRef<'a, Q> {
        ValRef {
            ptr: self.ptr,
            pred: PhantomData,
        }
    }
}

impl<'a, P> Deref for ValRef<'a, P> {
    type Target = ValId<P>;
    #[inline(always)]
    fn deref(&self) -> &ValId<P> {
        ValId::cast_arc_ref(self.ptr.borrow())
    }
}

impl<V: Value> VarId<V> {
    /// Construct a `ValId` from a given type implementing `Value`
    #[inline(always)]
    pub fn from_var(value: V) -> VarId<V> {
        value.into_valid().cast_valid_unchecked()
    }
    /// Directly cons a value of type `V` into a `ValId`
    #[inline(always)]
    pub fn from_var_direct(value: V) -> VarId<V> {
        ValId::new_direct(value.into_enum()).cast_valid_unchecked()
    }
}

impl VarRef<'_, Universe> {
    /// Assert that the type of a type is always a universe
    #[inline(always)]
    pub fn ty_ty_is_a_universe<T>(ty: &T) -> VarRef<Universe>
    where
        T: Type,
    {
        ty.ty().cast_ref_unchecked()
    }
}

impl TypeId {
    /// Construct a `TypeId` from a given type
    #[inline(always)]
    pub fn from_ty<T: Type>(ty: T) -> TypeId {
        ty.into_valid().cast_valid_unchecked()
    }
    /// Directly cons a value of type `T` into a `TypeId`
    #[inline(always)]
    pub fn from_ty_direct<T: Type>(ty: T) -> TypeId {
        ValId::new_direct(ty.into_enum()).cast_valid_unchecked()
    }
}

impl RegId {
    /// Construct a `RegId` from a given type
    #[inline(always)]
    pub fn from_reg<T: Register>(ty: T) -> RegId {
        ty.into_valid().cast_valid_unchecked()
    }
    /// Directly cons a value of type `T` into a `RegId`
    #[inline(always)]
    pub fn from_reg_direct<T: Register>(ty: T) -> RegId {
        ValId::new_direct(ty.into_enum()).cast_valid_unchecked()
    }
}

impl ReprId {
    /// Construct a `ReprId` from a given type
    #[inline(always)]
    pub fn from_repr<T: Representation>(ty: T) -> ReprId {
        ty.into_valid().cast_valid_unchecked()
    }
    /// Directly cons a value of type `T` into a `ReprId`
    #[inline(always)]
    pub fn from_repr_direct<T: Representation>(ty: T) -> ReprId {
        ValId::new_direct(ty.into_enum()).cast_valid_unchecked()
    }
}

impl<P> Copy for ValRef<'_, P> {}

impl<'a, P> Clone for ValRef<'a, P> {
    fn clone(&self) -> ValRef<'a, P> {
        ValRef {
            ptr: self.ptr,
            pred: PhantomData,
        }
    }
}

impl<P> Eq for ValId<P> {}

impl<'a, P> Eq for ValRef<'a, P> {}

impl<P, Q> PartialEq<ValId<Q>> for ValId<P> {
    #[inline(always)]
    fn eq(&self, other: &ValId<Q>) -> bool {
        Arc::ptr_eq(&self.ptr, &other.ptr)
    }
}

impl<'a, P, Q> PartialEq<ValRef<'a, Q>> for ValId<P> {
    #[inline(always)]
    fn eq(&self, other: &ValRef<'a, Q>) -> bool {
        ArcBorrow::ptr_eq(self.ptr.borrow_arc(), other.ptr)
    }
}

impl<'a, P, Q> PartialEq<ValId<Q>> for ValRef<'a, P> {
    #[inline(always)]
    fn eq(&self, other: &ValId<Q>) -> bool {
        ArcBorrow::ptr_eq(self.ptr, other.ptr.borrow_arc())
    }
}

impl<'a, 'b, P, Q> PartialEq<ValRef<'a, Q>> for ValRef<'b, P> {
    #[inline(always)]
    fn eq(&self, other: &ValRef<'a, Q>) -> bool {
        ArcBorrow::ptr_eq(self.ptr, other.ptr)
    }
}

impl<P> Hash for ValId<P> {
    #[inline(always)]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        Arc::as_ptr(&self.ptr).hash(hasher)
    }
}

impl<'a, P> Hash for ValRef<'a, P> {
    #[inline(always)]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        ArcBorrow::into_raw(self.ptr).hash(hasher)
    }
}

impl<P> Ord for ValId<P> {
    #[inline(always)]
    fn cmp(&self, other: &ValId<P>) -> Ordering {
        Arc::as_ptr(&self.ptr).cmp(&Arc::as_ptr(&other.ptr))
    }
}

impl<'a, P> Ord for ValRef<'a, P> {
    #[inline(always)]
    fn cmp(&self, other: &ValRef<'a, P>) -> Ordering {
        ArcBorrow::into_raw(self.ptr).cmp(&ArcBorrow::into_raw(other.ptr))
    }
}

impl<P, Q> PartialOrd<ValId<Q>> for ValId<P> {
    #[inline(always)]
    fn partial_cmp(&self, other: &ValId<Q>) -> Option<Ordering> {
        Arc::as_ptr(&self.ptr).partial_cmp(&Arc::as_ptr(&other.ptr))
    }
}

impl<'a, P, Q> PartialOrd<ValRef<'a, Q>> for ValId<P> {
    #[inline(always)]
    fn partial_cmp(&self, other: &ValRef<'a, Q>) -> Option<Ordering> {
        Arc::as_ptr(&self.ptr).partial_cmp(&ArcBorrow::into_raw(other.ptr))
    }
}

impl<'a, P, Q> PartialOrd<ValId<Q>> for ValRef<'a, P> {
    #[inline(always)]
    fn partial_cmp(&self, other: &ValId<Q>) -> Option<Ordering> {
        ArcBorrow::into_raw(self.ptr).partial_cmp(&Arc::as_ptr(&other.ptr))
    }
}

impl<'a, 'b, P, Q> PartialOrd<ValRef<'a, Q>> for ValRef<'b, P> {
    #[inline(always)]
    fn partial_cmp(&self, other: &ValRef<'a, Q>) -> Option<Ordering> {
        ArcBorrow::into_raw(self.ptr).partial_cmp(&ArcBorrow::into_raw(other.ptr))
    }
}
