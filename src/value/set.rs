/*!
A set of `rain` values
*/
use super::*;
use cons::flurry_cons::Cache;
use cons::{Cons, Uncons};
use lazy_static::lazy_static;
use pour::{BinaryResult, ConsCtx, IdSet, InnerMap, RadixKey};
use std::cmp::Ordering;
use std::hash::{Hash, Hasher};

/// A set of `rain` values
#[derive(Debug, Clone, Eq, Default)]
#[repr(transparent)]
pub struct ValSet(IdSet<ValId>);

impl ValSet {
    /// The empty set of `rain` values, as a constant
    pub const EMPTY: ValSet = ValSet(IdSet::EMPTY);
    /// Create a new `ValSet` consisting of a singleton
    #[inline(always)]
    pub fn singleton(value: ValId) -> ValSet {
        ValSet(IdSet::singleton_in(value, (), &mut GlobalValSetCache))
    }
    /// Get this `ValSet` as a pointer
    #[inline(always)]
    pub fn as_ptr(&self) -> *const u8 {
        self.0.as_ptr() as *const u8
    }
    /// Get the size of this `ValSet`
    pub fn len(&self) -> usize {
        self.0.len()
    }
    /// Get whether this `ValSet` is empty
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    /// Insert a new `ValId` into this `ValSet`, returning whether any changes were made
    pub fn insert(&mut self, value: ValId) -> bool {
        self.0.insert_in(value, (), &mut GlobalValSetCache)
    }
    /// Insert a new `ValId` into this `ValSet`
    pub fn inserted(&self, value: ValId) -> ValSet {
        self.try_inserted(value).unwrap_or_else(|| self.clone())
    }
    /// Insert a new `ValId` into this `ValSet`, yielding a new one if any change was made
    pub fn try_inserted(&self, value: ValId) -> Option<ValSet> {
        self.0
            .inserted_in(value, (), &mut GlobalValSetCache)
            .map(ValSet)
    }
    /// Remove a `ValId` from this `ValSet`, returning whether any changes were made
    pub fn remove(&mut self, value: &ValId) -> bool {
        self.0.remove_in(value, &mut GlobalValSetCache)
    }
    /// Remove a `ValId` from this `ValSet`
    pub fn removed(&self, value: &ValId) -> ValSet {
        self.try_removed(value).unwrap_or_else(|| self.clone())
    }
    /// Remove a `ValId` from this `ValSet`, yielding a new one if any change was made
    pub fn try_removed(&self, value: &ValId) -> Option<ValSet> {
        self.0.removed_in(value, &mut GlobalValSetCache).map(ValSet)
    }
    /// Take the union of this `ValSet` with another.
    pub fn union(&self, other: &ValSet) -> ValSet {
        self.try_union(other).unwrap_or_clone(self, other)
    }
    /// Take the union of this `ValSet` with another, yielding a `BinaryResult`
    pub fn try_union(&self, other: &ValSet) -> BinaryResult<ValSet> {
        self.0
            .union_in(&other.0, &mut GlobalValSetCache)
            .map(ValSet)
    }
    /// Take the intersection of this `ValSet` with another.
    pub fn intersect(&self, other: &ValSet) -> ValSet {
        self.try_intersect(other).unwrap_or_clone(self, other)
    }
    /// Take the intersection of this `ValSet` with another, yielding a `BinaryResult`
    pub fn try_intersect(&self, other: &ValSet) -> BinaryResult<ValSet> {
        self.0
            .intersect_in(&other.0, &mut GlobalValSetCache)
            .map(ValSet)
    }
    /// Take the symmetric difference of this `ValSet` with another.
    pub fn sym_diff(&self, other: &ValSet) -> ValSet {
        self.try_sym_diff(other).unwrap_or_clone(self, other)
    }
    /// Take the symmetric difference of this `ValSet` with another, yielding a `BinaryResult`
    pub fn try_sym_diff(&self, other: &ValSet) -> BinaryResult<ValSet> {
        self.0
            .sym_diff_in(&other.0, &mut GlobalValSetCache)
            .map(ValSet)
    }
    /// Take the complement of this `ValSet` with another.
    pub fn complement(&self, other: &ValSet) -> ValSet {
        self.try_complement(other).unwrap_or_clone(self, other)
    }
    /// Take the complement of this `ValSet` with another, yielding a `BinaryResult`
    pub fn try_complement(&self, other: &ValSet) -> BinaryResult<ValSet> {
        self.0
            .complement_in(&other.0, &mut GlobalValSetCache)
            .map(ValSet)
    }
}

/// The empty set of `rain` values
pub static EMPTY_SET: ValSet = ValSet::EMPTY;

impl Drop for ValSet {
    fn drop(&mut self) {
        if let Some(mut inner) = self.0.clear() {
            VALSET_CACHE.gc(&mut inner);
        }
    }
}

impl RadixKey for ValId {
    type PatternType = u64;
    type DepthType = u8;
    #[inline(always)]
    fn pattern(&self, _depth: u8) -> u64 {
        self.as_ptr() as usize as u64
    }
}

impl PartialEq for ValSet {
    fn eq(&self, other: &ValSet) -> bool {
        self.0.ptr_eq(&other.0)
    }
}

impl PartialOrd for ValSet {
    fn partial_cmp(&self, other: &ValSet) -> Option<Ordering> {
        self.as_ptr().partial_cmp(&other.as_ptr())
    }
}

impl Ord for ValSet {
    fn cmp(&self, other: &ValSet) -> Ordering {
        self.as_ptr().cmp(&other.as_ptr())
    }
}

impl Hash for ValSet {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.0.as_ptr().hash(hasher)
    }
}

/// A singleton representing global cache used by hash-consed `ValSet`s. Can be used as a cache for `IdSet`s of `ValId`s
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct GlobalValSetCache;

impl ConsCtx<ValId, ()> for GlobalValSetCache {
    fn cons_arc(&mut self, arc: &Arc<InnerMap<ValId, ()>>) -> Arc<InnerMap<ValId, ()>> {
        VALSET_CACHE.cons(arc.clone())
    }
    fn cons(&mut self, inner: InnerMap<ValId, ()>) -> Arc<InnerMap<ValId, ()>> {
        VALSET_CACHE.cons(inner)
    }
}

lazy_static! {
    /// The global cache for sets of `ValId`
    pub static ref VALSET_CACHE: Cache<Arc<InnerMap<ValId, ()>>> = Cache::new();
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::primitive::scalar::*;

    #[test]
    fn valset_sanity_check() {
        assert_eq!(ValSet::EMPTY, EMPTY_SET);
        assert!(EMPTY_SET.is_empty());
        assert_eq!(EMPTY_SET.len(), 0);
        let bool_set = ValSet::singleton(BOOL_TY.clone_as_valid());
        assert!(!bool_set.is_empty());
        assert_eq!(bool_set.len(), 1);
        let bool_set_2 = ValSet::singleton(BOOL_TY.clone_as_valid());
        assert_eq!(bool_set, bool_set_2);
        assert_eq!(bool_set.as_ptr(), bool_set_2.as_ptr());
        assert_eq!(bool_set.union(&bool_set_2), bool_set);
        let mut bool_val_set = ValSet::singleton(TRUE.clone_as_valid());
        assert!(!bool_val_set.is_empty());
        assert_eq!(bool_val_set.len(), 1);
        bool_val_set.insert(FALSE.clone_as_valid());
        assert_eq!(bool_val_set.len(), 2);
        let all_bool_set = bool_val_set.union(&bool_set);
        assert_eq!(all_bool_set.len(), 3);
        assert_ne!(all_bool_set, bool_val_set);
        assert_eq!(all_bool_set.removed(BOOL_TY.as_valid()), bool_val_set);
    }
}
