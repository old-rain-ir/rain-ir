/*!
Normalization, evaluation, and path typing (for use in proofs)
*/

use crate::function::*;
use crate::value::*;

mod path;
pub use path::*;