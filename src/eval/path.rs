/*!
Identity (path) types and associated proofs
*/
use super::*;

/// The identity type family associated with a given type
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Id {
    /// The type being considered
    base_ty: TypeId,
    /// The type of this family
    ty: VarId<Pi>,
}

/// The reflexivity axiom associated with a given type
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Refl {
    /// The type being considered
    base_ty: TypeId,
    /// The type of this axiom
    ty: VarId<Pi>,
}
